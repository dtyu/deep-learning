#!/usr/bin/env python

import os.path
import tensorflow as tf
import numpy as np

# Parameters
learning_rate = 0.001
training_epochs = 20 
batch_size = 100
display_step = 1

# Model saver
model_path = './many_rings_model.ckpt'

# Network Parameters
n_hidden_1 = 256 # 1st layer num neurons 
n_hidden_2 = 64 # 2nd layer num neurons 
n_hidden_3 = 32 # 3rd layer num neurons
n_input = 1024 # Original input size 32*32 NSLS-II images. 
n_classes = 1 # Classification with two independent features. 
n_pixels = 1024 # Number of Pixels before entering the densely connected layer.

# tf Graph input
x = tf.placeholder(tf.float32, shape=[None, n_input])
y = tf.placeholder(tf.float32, shape=[None, n_classes])

# Create model
def multilayer_perceptron(_X, _weights, _biases):
    # To apply the layer, we reshape x to a 4d tensor
    # and perform max pooling on the image.

    #x_image = tf.reshape(_X, [-1, 256, 256, 1])
    #x_pool1 = tf.nn.max_pool(x_image, ksize=[1,8,8,1], strides=[1,8,8,1], padding='SAME')
    #x_pool1_flat = tf.reshape(x_pool1, [-1, n_pixels])

    layer_1 = tf.nn.sigmoid(tf.add(tf.matmul(_X, _weights['h1']), _biases['b1'])) #Hidden layer with RELU activation
    layer_2 = tf.nn.sigmoid(tf.add(tf.matmul(layer_1, _weights['h2']), _biases['b2'])) #Hidden layer with RELU activation
    layer_3 = tf.nn.sigmoid(tf.add(tf.matmul(layer_2, _weights['h3']), _biases['b3'])) #Hidden layer with RELU activation
    return tf.sigmoid(tf.matmul(layer_3, _weights['out']) + _biases['out'])

# Store layers weight & bias
weights = {
    'h1': tf.Variable(tf.random_normal([n_pixels, n_hidden_1])),
    'h2': tf.Variable(tf.random_normal([n_hidden_1, n_hidden_2])),
    'h3': tf.Variable(tf.random_normal([n_hidden_2, n_hidden_3])),
    'out': tf.Variable(tf.random_normal([n_hidden_3, n_classes]))
}
biases = {
    'b1': tf.Variable(tf.random_normal([n_hidden_1])),
    'b2': tf.Variable(tf.random_normal([n_hidden_2])),
    'b3': tf.Variable(tf.random_normal([n_hidden_3])),
    'out': tf.Variable(tf.random_normal([n_classes]))
}

# Construct model.
pred = multilayer_perceptron(x, weights, biases)

# Loss and optimizer.
#cost = -tf.reduce_sum(y * tf.log(pred)) # Cross Entropy loss 
cost = tf.reduce_sum(tf.pow(pred - y, 2)) # L2 loss
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

# Test model
prediction = tf.cast(tf.round(pred), "int64")
correct_prediction = tf.equal(prediction, tf.cast(y, "int64")) 
# Calculate accuracy
accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))

# Load the NSLS-II image data.
# Separate the labels and the data.
# The perform concatenation.
data_all = np.load('../many_rings_all.npy')
features = data_all[:,0:1024]
labels = data_all[:,1024:]
features = np.where(features > 1.0, np.log10(features), 0)
data_all = np.concatenate((features, labels), axis = 1)

# For cross validation purpose.
data = data_all[0:18000,]
test = data_all[18000:,]
#data = data_all

# Initializing the variables
init = tf.initialize_all_variables()
saver = tf.train.Saver()

# Launch the graph
with tf.Session() as sess:
    sess.run(init)
    if os.path.exists(model_path):
        saver.restore(sess, model_path)
        print "Model Restored!"

    # Training cycle
    for epoch in range(training_epochs):
        np.random.shuffle(data)
        avg_cost = 0.
        total_batch = int(data.shape[0]/batch_size)
        # Loop over all batches
        for i in range(total_batch):
            batch_xs = data[(0 + i * batch_size):(batch_size + i * batch_size),0:n_input]
            batch_ys = data[(0 + i * batch_size):(batch_size + i * batch_size),n_input:]
            # Fit training using batch data
            sess.run(optimizer, feed_dict={x: batch_xs, y: batch_ys})
            # Compute average loss
            avg_cost += sess.run(cost, feed_dict={x: batch_xs, y: batch_ys})/total_batch
        # Display logs per epoch step
        if epoch % display_step == 0:
            print "Epoch:", '%04d' % (epoch+1), "cost=", "{:.9f}".format(avg_cost)

        # Display the training set accuracy
        print "Training set accuracy:", accuracy.eval({x: data[:,0:n_input], y: data[:,n_input:]})
        print "Testing set accuracy:", accuracy.eval({x: test[:,0:n_input], y: test[:,n_input:]})

    save_path = saver.save(sess, model_path)
    print("Model saved in file: %s" % save_path)

    # Use TensorFlow eval function to generate the predicted
    # labels and then store them in a numpy array.
    results_pred = prediction.eval({x: data_all[:,0:n_input]})
    np.save("MLPprediction.npy", results_pred)

    # Finish up and evaluate on the test dataset.
    print "Optimization Finished!"
    print "Testing set accuracy:", accuracy.eval({x: test[:,0:n_input], y: test[:,n_input:]})

