#!/usr/bin/env python

import os.path
import tensorflow as tf
import numpy as np

# Learning Parameters
learning_rate = 0.001
training_epochs = 20 
batch_size = 100 
display_step = 1 

# Model saver
model_path = './many_rings_CNN_model.ckpt'

# Network Parameters
n_features = 8
n_input = 16 * 16 * n_features # Convolutional Layer output 
n_hidden_1 = 128 # 1st layer num features
n_hidden_2 = 32 # 2nd layer num features
n_classes = 1 # Prediction task numbers 

# tf Graph input.
x = tf.placeholder(tf.float32, shape=[None, 1024])
y = tf.placeholder(tf.float32, shape=[None, n_classes])

## convolation and pooling. 
def conv2d(x, W):
    return tf.nn.conv2d(x, W, strides=[1,1,1,1], padding='SAME')

# Create CNN model
def convolutional_nn(_X, _weights, _biases):
    # Image layer: 
    # 1. The original picture is shifted to 
    # 2. conv2d is then performed on the image and get 32*32*n_features 
    # 3. max_pool is performaed to 16*16*n_features
    # 4. Two densely connected layers with 128 and 32 neurons
    # with relu activation functions is then introduced in the model.

    #x_pool1 = tf.nn.max_pool(x_image, ksize=[1,8,8,1], strides=[1,8,8,1], padding='SAME')

    x_image = tf.reshape(_X, [-1, 32, 32, 1])
    h_conv1 = tf.nn.relu(conv2d(x_image, _weights['conv1']) + _biases['convb1'])
    x_pool2 = tf.nn.max_pool(h_conv1, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME')
    x_pool2_flat = tf.reshape(x_pool2, [-1, 16 * 16 * n_features])
    layer_1 = tf.nn.sigmoid(tf.add(tf.matmul(x_pool2_flat, _weights['h1']), _biases['b1']))
    layer_2 = tf.nn.sigmoid(tf.add(tf.matmul(layer_1, _weights['h2']), _biases['b2']))
    return tf.sigmoid(tf.matmul(layer_2, _weights['out']) + _biases['out'])

# Store weight & bias in a dict object.
weights = {
    'conv1': tf.Variable(tf.truncated_normal([5,5,1,n_features], stddev = 0.1)),
    'h1': tf.Variable(tf.random_normal([n_input, n_hidden_1])),
    'h2': tf.Variable(tf.random_normal([n_hidden_1, n_hidden_2])),
    'out': tf.Variable(tf.random_normal([n_hidden_2, n_classes]))
}
biases = {
    'convb1': tf.Variable(tf.constant(0.1, shape = [n_features])),
    'b1': tf.Variable(tf.random_normal([n_hidden_1])),
    'b2': tf.Variable(tf.random_normal([n_hidden_2])),
    'out': tf.Variable(tf.random_normal([n_classes]))
}

# Construct model.
pred = convolutional_nn(x, weights, biases)

# Loss and optimizer.
#cost = -tf.reduce_sum(y * tf.log(pred)) # Cross Entropy loss 
cost = tf.reduce_sum(tf.pow(pred - y, 2)) # L2 loss
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

# Test model
prediction = tf.cast(tf.round(pred), "int64")
correct_prediction = tf.equal(prediction, tf.cast(y, "int64"))
# Calculate accuracy
accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))

# Load the NSLS-II image data.
data_all = np.load('../../many_rings_all.npy')
features = data_all[:,0:1024]
labels = data_all[:,1024:]
features = np.where(features > 1.0, np.log10(features), 0)
data_all = np.concatenate((features, labels), axis = 1)

# For cross validation purpose.
data = data_all[0:18000,]
test = data_all[18000:,]
#data = data_all

# Initializing the variables
init = tf.initialize_all_variables()
saver = tf.train.Saver()

# Launch the graph
with tf.Session() as sess:
    sess.run(init)
    if os.path.exists(model_path):
        saver.restore(sess, model_path)
        print "Model Restored!"

    # Training cycle
    for epoch in range(training_epochs):
        np.random.shuffle(data)
        avg_cost = 0.
        total_batch = int(data.shape[0]/batch_size)
        # Loop over all batches
        for i in range(total_batch):
            batch_xs = data[(0 + i * batch_size):(batch_size + i * batch_size),0:1024]
            batch_ys = data[(0 + i * batch_size):(batch_size + i * batch_size),1024:]
            # Fit training using batch data
            sess.run(optimizer, feed_dict={x: batch_xs, y: batch_ys})
            # Compute average loss
            avg_cost += sess.run(cost, feed_dict={x: batch_xs, y: batch_ys})/total_batch
        # Display logs per epoch step
        if epoch % display_step == 0:
            print "Epoch:", '%04d' % (epoch+1), "cost=", "{:.9f}".format(avg_cost)
        # Display the training set accuracy
        print "Training set accuracy:", accuracy.eval({x: data[:,0:1024], y: data[:,1024:]})
        print "Testing set accuracy:", accuracy.eval({x: test[:,0:1024], y: test[:,1024:]})


    save_path = saver.save(sess, model_path)
    print("Model saved in file: %s" % save_path)

    # Use the TensorFlow eval function to generate the predicted
    # labels and then store them in a numpy array.
    results_pred = prediction.eval({x: data_all[:,0:1024]})
    np.save("CNNprediction.npy", results_pred)

    # Finish up and evaluate on the test dataset.
    print "Optimization Finished!"
    print "Testing set accuracy:", accuracy.eval({x: test[:,0:1024], y: test[:,1024:]})

