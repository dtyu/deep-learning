#!/usr/bin/env python

import tensorflow as tf
import numpy as np

# Parameters
learning_rate = 0.001
training_epochs = 100 
batch_size = 100
display_step = 1

# Network Parameters
n_hidden_1 = 128 # 1st layer num features
n_hidden_2 = 32 # 2nd layer num features
n_input = 65536 # MNIST data input (img shape: 28*28)
n_classes = 2 # MNIST total classes (0-9 digits)

# tf Graph input
x = tf.placeholder(tf.float32, shape=[None, n_input])
y = tf.placeholder(tf.float32, shape=[None, 2])

# Create model
def multilayer_perceptron(_X, _weights, _biases):
    # To apply the layer, we reshape x to a 4d tensor
    # and perform max pooling on the image.
    x_image = tf.reshape(x, [-1, 256, 256, 1])
    x_pool1 = tf.nn.max_pool(x_image, ksize=[1,8,8,1], strides=[1,8,8,1], padding='SAME')
    layer_1 = tf.nn.relu(tf.add(tf.matmul(_X, _weights['h1']), _biases['b1'])) #Hidden layer with RELU activation
    layer_2 = tf.nn.relu(tf.add(tf.matmul(layer_1, _weights['h2']), _biases['b2'])) #Hidden layer with RELU activation
    return tf.sigmoid(tf.matmul(layer_2, _weights['out']) + _biases['out'])

# Store layers weight & bias
weights = {
    'h1': tf.Variable(tf.random_normal([n_input, n_hidden_1])),
    'h2': tf.Variable(tf.random_normal([n_hidden_1, n_hidden_2])),
    'out': tf.Variable(tf.random_normal([n_hidden_2, n_classes]))
}
biases = {
    'b1': tf.Variable(tf.random_normal([n_hidden_1])),
    'b2': tf.Variable(tf.random_normal([n_hidden_2])),
    'out': tf.Variable(tf.random_normal([n_classes]))
}

# Construct model.
pred = multilayer_perceptron(x, weights, biases)

# Loss and optimizer.
#cost = -tf.reduce_sum(y * tf.log(pred)) # Cross Entropy loss 
cost = tf.reduce_sum(tf.pow(pred - y, 2)) # L2 loss
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

# Load the NSLS-II image data.
images = np.load("images.npy")
labels = np.load("labels.npy")
data = np.concatenate((images,labels), axis = 1)

# For cross validation purpose.
test = data[300:,]
data = data[1:300,]

# Initializing the variables
init = tf.initialize_all_variables()
saver = tf.train.Saver()

# Launch the graph
with tf.Session() as sess:
    sess.run(init)
    saver.restore(sess, "./CVmodel.ckpt")
    print "Model Restored!"

    # Training cycle
    for epoch in range(training_epochs):
        np.random.shuffle(data)
        avg_cost = 0.
        total_batch = int(images.shape[0]/batch_size)
        # Loop over all batches
        for i in range(total_batch):
            batch_xs = data[(0 + i * batch_size):(batch_size + i * batch_size),0:65536]
            batch_ys = data[(0 + i * batch_size):(batch_size + i * batch_size),65536:]
            # Fit training using batch data
            sess.run(optimizer, feed_dict={x: batch_xs, y: batch_ys})
            # Compute average loss
            avg_cost += sess.run(cost, feed_dict={x: batch_xs, y: batch_ys})/total_batch
        # Display logs per epoch step
        if epoch % display_step == 0:
            print "Epoch:", '%04d' % (epoch+1), "cost=", "{:.9f}".format(avg_cost)

    save_path = saver.save(sess, "./CVmodel.ckpt")
    print("Model saved in file: %s" % save_path)

    print "Optimization Finished!"

    # Test model
    correct_prediction = tf.equal(tf.cast(tf.round(pred), "int64"), tf.cast(y, "int64")) 
    # Calculate accuracy
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
    print "Accuracy:", accuracy.eval({x: test[:,0:65536], y: test[:,65536:]})

