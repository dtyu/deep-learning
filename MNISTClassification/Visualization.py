#!/usr/bin/env python

# This file visualize some of the MNIST dataset image.
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt 
import numpy as np

# Treat it as a square image.
image = mnist.train.images[1,]
n = np.sqrt(len(image)).astype(int)
image = image.reshape(n,n)

#fig=plt.imshow(image, cmap='Greys_r')
#fig.axes.get_xaxis().set_visible(False)
#fig.axes.get_yaxis().set_visible(False)

#plt.imshow(image, cmap='Greys_r')
fig=plt.imshow(image, cmap='Greys')
fig.axes.get_xaxis().set_visible(False)
fig.axes.get_yaxis().set_visible(False)
plt.savefig('image.eps')
